import mongoose from 'mongoose';

mongoose.Promise = global.Promise

export const connect = () => mongoose.connect('mongodb://db/jobapp', 
{ useNewUrlParser: true, useFindAndModify: false, useCreateIndex: true })

// export const connect = () =>
//          mongoose.connect(
//            "mongodb+srv://hollajobs:siliconh2019@hollajobs-2u8a3.mongodb.net/test?retryWrites=true&w=majority",
//            {
//              useNewUrlParser: true,
//              useFindAndModify: false,
//              useCreateIndex: true
//            }
//          );