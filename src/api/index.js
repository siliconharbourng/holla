import express from 'express';
import { authRouter } from "./resources/auth";
import { bookingRouter } from './resources/booking';
import { usersRouter } from './resources/users';
import { locationRouter } from './resources/location';
import { ratingRouter } from './resources/rating';
import { bookChatRouter } from './resources/bookingChat';
import { walletRouter } from './resources/wallet';

export const restApIRouter = express.Router();

restApIRouter.use('/auth', authRouter);
restApIRouter.use('/booking', bookingRouter);
restApIRouter.use('/booking/chat', bookChatRouter);
restApIRouter.use('/user', usersRouter);
restApIRouter.use('/user/location', locationRouter);
restApIRouter.use('/rating', ratingRouter);
restApIRouter.use('/transaction', walletRouter);