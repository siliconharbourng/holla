import Passport from 'passport';
import PassportJwt from 'passport-jwt';
import { devConfig } from '../../config/env/developement';
import User from '../resources/users/users.model'

export const configJWTStrategy = () => {
    const opts = {
        jwtFromRequest: PassportJwt.ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: devConfig.secret,
    };
    Passport.use(new PassportJwt.Strategy(opts, (payload, done) => {
        User.findOne({_id: payload.id}, (err,user) => {
            if (err) {
                return done(err)
            }
            if (user) {
                return done(null, user);
            }
            return done(null, false)
        })
    }))
}