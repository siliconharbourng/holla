module.exports = function (io, User, _) {
    const user = new User();
    io.on('connection', (socket) => {
        socket.on('refresh', (data) => {
            console.log(data)
            io.emit('refreshpage', (data));
        });

        socket.on('location change', (data) => {
            console.log('location')
            io.emit('new_location', (data))
        });

        socket.on('sending', (data) => {
            io.emit('received', (data))
        });

        socket.on('notify', data => {
            console.log(data)
            io.emit('notification', data)
        })

        socket.on('online', data => {
            socket.join(data.room);
            user.EnterRoom(socket.id, data.user, data.room);
            const list = user.GetList(data.room);
            io.emit('usersOnline', _.uniq(list))
        });

        socket.on('disconnect', () => {
            const userData = user.RemoveUser(socket.id)
            if (userData) {
                const userArray = user.GetList(userData.room)
                const arr = _.uniq(userArray);
                _.remove(arr, n => n === userData.name)
                console.log(arr);
                io.emit('usersOnline', arr);
            }
        })
    })
}