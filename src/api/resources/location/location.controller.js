import User from '../users/users.model';
import jwtDecode from 'jwt-decode';

export default {
    async location(req, res) {
    try {
      const token = req.headers.authorization.split('bearer ')[1];
      const decoded = jwtDecode(token);
            await User.updateOne(
                {
                    _id: decoded.id
                },
                {
                  $set: { 'my_location.coordinates': req.body.coordinates, 'my_location.type': "Point" },
                }
                );
                return res.json({message: "location Updated"})
            } catch (error) {
            console.error(error);
            return res.status(500).json(error);
        }  
    },

    async myLocation(req, res) {
        try {
            const { id } = req.params;
            const worker = await User.findOne({ _id: id})
            const location = worker.location;
            return res.status(200).json(location)
        } catch (error) {
            console.error(error);
            return res.status(500).json(error)   
        }
    },

    async workers(req, res) {
      try {
        const worker = await User.find({rights: "worker"}).select('my_location.coordinates');
        return res.status(200).json(worker);
      } catch (error) {
        console.error(error);
        return res.status(500).json(error);  
      }
  },
    
  async nearby_workers(req, res, next) {
        console.log(req.query)
    try {
      // const { distance, coordinates } = req.body;
      // const worker = await User.find({
      //   rights: "worker", my_location: {
      //     $near: {
      //       $maxDistance: distance,
      //       $geometry: {
      //         type: "Point",
      //         coordinates: coordinates
      //       }
      //   }
      // } }).select('skills my_location')
      res.status(200).json(req);
    } catch (error) {
      console.error(error);
      return res.status(500).json(error); 
    }
  },

  async near_workers(req, res) {
    try {
      const { distance, lat, lng } = req.query;
      await User.ensureIndexes({ 'my_location': "2dsphere" });
      const worker = await User.find({
        rights: "worker",
        isKYC_verified: false,
        // my_location: {
        //   $near: {
        //     $maxDistance: distance,
        //     $geometry: {
        //       type: "Point",
        //       coordinates: [parseFloat(lng), parseFloat(lat)]
        //     }
        // }
        // }
      }).select('verified_skill my_location')
      res.status(200).json(worker);
    } catch (error) {
      console.error(error);
      return res.status(500).json(error); 
    }
  }
}