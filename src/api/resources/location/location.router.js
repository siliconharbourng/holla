import express from 'express';
import locationController from './location.controller';
import passport from 'passport';

export const locationRouter = express.Router();

locationRouter.put('/', passport.authenticate('jwt', {session:false}), locationController.location);
locationRouter.get('/:id', locationController.myLocation);
// locationRouter.get('/workers', passport.authenticate('jwt', {session:false}), locationController.near_workers);
locationRouter.get('/nearby/worker', locationController.near_workers);