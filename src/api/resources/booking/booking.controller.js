import authService from "../auth/auth.service";
import Booking from "./booking.model"
import User from "../users/users.model";
import jwtDecode from 'jwt-decode';




export default {
    async requestWorker(req, res) {
        try {            
            // const { value, error } = authService.validateBooking(req.body)
            // if (error) {
            //     res.status(400).json(error)
            // }
            const { location_address, skill, worker, coordinate } = req.body;
            const token = req.headers.authorization.split('bearer ')[1];
            const decoded = jwtDecode(token);
            
            const booked = await Booking.findOne({ isAvailable: true, workersId: worker })
            const request = await Booking.find({ isAvailable: true, seekersId: decoded.id }) 
            if (request.length >= 5) {                
                return res.status(400).json({ message: "You cannot request for more than 5 workers at a time" })
            }
            if (booked) {
                return res.status(400).json({ message: "The worker you have requested is currently engaged" })
            }
            if (!worker) {
                return res.status(400).json({ message: "The person you have requested is not looking for a job" } )
            }
            const book = await Booking.create({
                seekersId: decoded.id,
                jobType: skill,
                workersId: worker,
                jobLocation: {
                    type: 'Point',
                    coordinates: coordinate
                },
                jobAddress: location_address
            });
            return res.status(200).json(book)

            await User.updateOne({
                _id: item._id
            },
            {
                $push: {
                    notifications: {
                        $each: [
                            {
                                senderId: value.seekersId,
                                message: `${value.sender} has requested for your service`,
                                updated: Date.now()
                            }
                        ],
                        $position: 0
                    },
                    jobOffer: {
                        $each: [
                            {
                                jobId: book._id
                            }
                        ],
                        $position: 0
                    }
                }
            }
            )
            await User.updateOne({
                _id: value.seekersId
            },
            {
                $push: {
                    workersRequested: {
                        $each: [
                            {
                                requestId: book._id
                            }
                        ],
                        $position: 0
                    }
                }
            }
            )
            res.status(200).json(book); 
        } catch (error) {
            console.error(error);
            return res.status(500).json(error)
        }
    },

    async availableRequest(req, res){
        try {
            const token = req.headers.authorization.split('bearer ')[1];
            const decoded = jwtDecode(token);
            const request = await Booking.findOne({
                $or: [
                    { isAvailable: true, seekersId: decoded.id },
                    { isAvailable: true, workersId: decoded.id }
                ]
            }).populate('seekersId').populate('workersId');
            res.status(200).json(request)
        } catch (error) {
            console.error(error);
            return res.status(500).send(error)
        }
    },

    async updateRequest(req, res) {
        try {
            const token = req.headers.authorization.split('bearer ')[1];
            const decoded = jwtDecode(token);
            // const { value, error } = authService.validateStatus(req.body)
            // if (error) {
            //     res.status(400).json(error)
            // }
            const body = {
                status: req.body.status,
                updated: Date.now()
            }
            const right = await Booking.findOne({ isAvailable: true, seekersId: decoded.id }) ||
            await Booking.findOne({ isAvailable: true, workersId: decoded.id })
            if (!right) {
                return res.status(400).json({ message: "No job is currently available" })
            }            
            // if (value.status == 'accepted' && req.params.id != right.workersId) {
            //     return res.status(400).json({ message: "you are not authorized for this" }) 
            // }
            // if (value.status == 'arrived' && req.params.id != right.workersId) {
            //     return res.status(400).json({ message: "you are not authorized for this" }) 
            // }
            // if (value.status == 'inprogress' && req.params.id != right.seekersId ) {
            //     return res.status(400).json({ message: "you are not authorized for this" })
            // }
            // if (value.status == 'confirmjob' && req.params.id != right.seekersId ) {
            //     return res.status(400).json({ message: "you are not authorized for this" })
            // }
            // if (value.status == 'startjob' && req.params.id != right.workersId ) {
            //     return res.status(400).json({ message: "you are not authorized for this" })
            // }
            // if (value.status == 'pending' ) {
            //     return res.status(400).json({ message: "these action is not allowed" })
            // }
            // if (value.status == 'cancel') {
            //     return res.status(400).json({ message: "these action is not allowed" })
            // }
            // if (value.status == 'worker complete' && req.params.id != right.workersId) {
            //     return res.status(400).json({ message: "you are not authorized for this" })
            // }
            // if (value.status == 'job completed' && req.params.id != right.seekersId) {
            //     return res.status(400).json({ message: "you are not authorized for this" })
            // }
            // if (value.status == 'rejected') {
            //     return res.status(400).json({ message: "these action is not allowed" })
            // }
            
            const update = await Booking.findOneAndUpdate({ isAvailable: true, seekersId: decoded.id}, body, {new: true}) ||
            await Booking.findOneAndUpdate({ isAvailable: true, workersId: decoded.id}, body, {new: true})
            if (!update) {
                return res.status(400).json({message: "No job is currently available"})
            } else {
                // if (value.status == 'accepted') {
                //     await User.updateOne({
                //         _id: update.seekersId,
                //     },
                //         {
                //             $push: {
                //                 notifications: {
                //                     $each: [
                //                         {
                //                             senderId: update.workersId,
                //                             message: `${update.receiver} has accepted your offer`,
                //                             updated: Date.now()
                //                         }
                //                     ],
                //                     $position: 0
                //                 }
                //             }
                //         })
                    
                // }
                // if (value.status == 'arrived') {
                //     await User.updateOne({
                //         _id: update.seekersId
                //     },
                //         {
                //             $push: {
                //                 notifications: {
                //                     $each: [
                //                         {
                //                             senderId: update.workersId,
                //                             message: `${update.receiver} has arrived the Location of appointment`,
                //                             updated: Date.now()
                //                         }
                //                     ],
                //                     $position: 0
                //                 }
                //             }
                //         })
                // }
                // if (value.status == 'confirmjob') {
                //     await User.updateOne({
                //         _id: update.workersId
                //     },
                //         {
                //             $push: {
                //                 notifications: {
                //                     $each: [
                //                         {
                //                             senderId: update.seekersId,
                //                             message: `${update.sender} has agreed to your terms and has proceeded to start the job`,
                //                             updated: Date.now()
                //                         }
                //                     ],
                //                     $position: 0
                //                 }
                //             }

                //         })
                // }
                // if (value.status == 'job completed') {
                //     await User.updateOne({
                //         _id: update.seekersId
                //     },
                //         {
                //             $push: {
                //                 notifications: {
                //                     $each: [
                //                         {
                //                             senderId: update.workersId,
                //                             message: `${update.receiver} has Completed the task give`,
                //                             updated: Date.now()
                //                         }
                //                     ],
                //                     $position: 0
                //                 }
                //             }

                //         })
                // }
                res.status(200).json(update)
            }
        } catch (error) {
            console.error(error);
            return res.status(500).send(error)
        }
    },

    async cancelrequest(req, res) {
        try {
            const { id } = req.params
            const { value, error } = authService.validateStatus(req.body)
            if (error) {
                res.status(400).json(error)
            }
            const body = {
                status: value.status,
                cancelReason: {
                    canceller: req.params.id,
                    reason: value.cancelReason.reason,
                    time: Date.now()
                },
                isAvailable: false,
                updated: Date.now()
            }
            const right = await Booking.findOne({ isAvailable: true, seekersId: id }) ||
                await Booking.findOne({ isAvailable: true, workersId: id })
            if (!right) {
                return res.status(400).json({ message: "No job is currently available" })
            }
            if (value.status == 'rejected' && req.params.id != right.workersId) {
                return res.status(400).json({ message: "you are not authorized for this" })
            }
            if (value.status == 'cancel' && req.params.id != right.seekersId) {
                return res.status(400).json({ message: "you are not authorized for this" })
            }
            if (value.status == 'pending') {
                return res.status(400).json({ message: "these action is not allowed" })
            }
            if (value.status == 'inprogress') {
                return res.status(400).json({ message: "these action is not allowed" })
            }
            if (value.status == 'completed') {
                return res.status(400).json({ message: "these action is not allowed" })
            }
            if (value.status == 'arrived') {
                return res.status(400).json({ message: "these action is not allowed" })
            }
            if (value.status == 'accepted') {
                return res.status(400).json({ message: "these action is not allowed" })
            }
            const update = await Booking.findOneAndUpdate({ isAvailable: true, seekersId: id }, body, { new: true }) ||
            await Booking.findOneAndUpdate({ isAvailable: true, workersId: id }, body, { new: true })
            if (!update) {
                return res.status(400).json({ message: "No job is currently available" })
            } else {
                if (value.status == 'rejected') {
                    await User.updateOne({
                        _id: update.seekersId,
                    },
                        {
                            $push: {
                                notifications: {
                                    $each: [
                                        {
                                            senderId: update.workersId,
                                            message: `${update.receiver} has Rejected your offer`,
                                            updated: Date.now()
                                        }
                                    ],
                                    $position: 0
                                }
                            }
                        })

                }
                if (value.status == 'cancel') {
                    await User.updateOne({
                        _id: update.workersId
                    },
                        {
                            $push: {
                                notifications: {
                                    $each: [
                                        {
                                            senderId: update.seekersId,
                                            message: `${update.sender} has cancelled the appointment`,
                                            updated: Date.now()
                                        }
                                    ],
                                    $position: 0
                                }
                            }
                        })
                }
                res.status(200).json({message:'Job has been cancelled', update})
            }
        } catch (error) {
            console.error(error);
            return res.status(500).json(error)
        }
    },

    async completerequest(req, res) {
        try {
            // const { id } = req.params
            // const { value, error } = authService.validateStatus(req.body)
            // if (error) {
            //     res.status(400).json(error)
            // }
            const token = req.headers.authorization.split('bearer ')[1];
            const decoded = jwtDecode(token);

            const right = await Booking.findOne({ isAvailable: true, seekersId: decoded.id })
            if (right && decoded.id == right.seekersId) {
                const body = {
                    status: "job completed",
                    isAvailable: false,
                    updated: Date.now()
                }
                const update = await Booking.findOneAndUpdate({ isAvailable: true, seekersId: decoded.id }, body, { new: true }) ||
                    await Booking.findOneAndUpdate({ isAvailable: true, workersId: decoded.id }, body, { new: true })
                if (!update) {
                    return res.status(400).json({ message: "No job is currently available" })
                } else {
                    await User.updateOne({
                        _id: update.seekersId,
                    },
                        {
                            $push: {
                                notifications: {
                                    $each: [
                                        {
                                            senderId: update.workersId,
                                            message: `You have successfully had a nice deal with ${update.receiver}`,
                                            updated: Date.now()
                                        }
                                    ],
                                    $position: 0
                                }
                            }
                        })
                    await User.updateOne({
                        _id: update.workersId,
                    },
                        {
                            $push: {
                                notifications: {
                                    $each: [
                                        {
                                            senderId: update.seekersId,
                                            message: `You have successfully completed the task ${update.sender} gave you`,
                                            updated: Date.now()
                                        }
                                    ],
                                    $position: 0
                                }
                            }
                        })
                    res.status(200).json(update)
                }     
            } else {
                return res.status(400).json({ message: "you are not authorized for this" })
            }
        } catch (error) {
            console.error(error);
            return res.status(500).json(error)
        }
    },

    async negotiatePrice(req, res) {
        try {
            const token = req.headers.authorization.split('bearer ')[1];
            const decoded = jwtDecode(token);
            
            const body = {
                    status: "negotiate",
                    payment: {
                        material: req.body.material,
                        labour: req.body.labour,
                        transportation: req.body.transportation,
                        others: req.body.others,
                        currency: 'NGN'
                    },
                    updated: Date.now()
            }
            const update = await Booking.findOneAndUpdate({ isAvailable: true, seekersId: decoded.id }, body, { new: true }) ||
                await Booking.findOneAndUpdate({ isAvailable: true, workersId: decoded.id }, body, { new: true })
            if (!update) {
                return res.status(400).json({ message: "No job is currently available" })
            } else {
                res.status(200).json(update)
             }
            
        } catch (error) {
            console.error(error);
            return res.status(500).json(error)
        }
    },

    async agreePrice(req, res) {
          try {
            const { id } = req.params
            const body = {
                    status: "startjob",
                    payment: {
                        method: req.body.method,
                    },
                    updated: Date.now()
            }
            const update = await Booking.findOneAndUpdate({ isAvailable: true, seekersId: id }, body, { new: true }) ||
                await Booking.findOneAndUpdate({ isAvailable: true, workersId: id }, body, { new: true })
            if (!update) {
                return res.status(400).json({ message: "No job is currently available" })
            } else {
                res.status(200).json(update)
             }
            
        } catch (error) {
            console.error(error);
            return res.status(500).json(error)
        }
    },
      
    async schedule(req, res) {
        try {
        const { id } = req.params
        const body = {
            status: "schedule",
            isAvailable: false,
            updated: Date.now()
        }
        const update = await Booking.findOneAndUpdate({ isAvailable: true, seekersId: id }, body, { new: true }) ||
            await Booking.findOneAndUpdate({ isAvailable: true, workersId: id }, body, { new: true })
        if (!update) {
            return res.status(400).json({ message: "No job is currently available" })
        } else {
            res.status(200).json(update)
            }
        
    } catch (error) {
        console.error(error);
        return res.status(500).json(error)
    }
    },
    
    async allJobs(req, res) {
        try {
            await Booking.find().then(resp => {
                res.status(200).json(resp)
            }).catch(err => {
                res.status(400).json(err)
            });
        } catch (error) {
            console.log(error);
            res.status(500).json(error);
        }
    },

    async userRequest(req, res){
        try {
            const token = req.headers.authorization.split('bearer ')[1];
            const decoded = jwtDecode(token);
            const request = await Booking.find({
                $or: [
                    { seekersId: decoded.id },
                    { workersId: decoded.id }
                ]
            }).populate('seekersId').populate('workersId');
            res.status(200).json(request)
        } catch (error) {
            console.error(error);
            return res.status(500).send(error)
        }
    },
}