import express from 'express';
import passport from 'passport';
import bookingController from './booking.controller'

export const bookingRouter = express.Router();

bookingRouter.post('/request', passport.authenticate('jwt', {session:false}), bookingController.requestWorker) // Create a job request
bookingRouter.get('/all-request', passport.authenticate('jwt', {session:false}), bookingController.allJobs) // all job request
bookingRouter.get('/all_request', bookingController.allJobs) // all job request
bookingRouter.get('/all_my_request', passport.authenticate('jwt', {session:false}), bookingController.userRequest) // all job request
bookingRouter.get('/request', passport.authenticate('jwt', {session:false}),  bookingController.availableRequest) // Get available jobs user
bookingRouter.patch('/request', passport.authenticate('jwt', {session:false}),  bookingController.updateRequest) // update available jobs 
bookingRouter.put('/request/:id/cancel',  bookingController.cancelrequest) // cancel a job request
bookingRouter.put('/request/complete', passport.authenticate('jwt', {session:false}), bookingController.completerequest) // successfull complete a job request
bookingRouter.put('/request/negotiate', passport.authenticate('jwt', {session:false}), bookingController.negotiatePrice)
bookingRouter.patch('/request/:id/agreeprice', bookingController.agreePrice)
bookingRouter.patch('/request/:id/schedule', bookingController.schedule)
