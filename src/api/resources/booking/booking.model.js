import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';

const { Schema } = mongoose;

const locateSchema = new mongoose.Schema({
  type: { type: String, enum: ["Point"], required: true },
  coordinates: {
    type: [Number],
    index: { type: "2dsphere" }
  }
});

const bookingSchema = new Schema({
  seekersId: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
  workersId: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
  jobType: {type: String, default: ""},
  reqesting_for: {
    name: {type: String},
    sex: { type: String},
    phone: { type: String},
  },
  status: {
    type: String,
    enum: [
      "pending", // only client can perform this action
      "accepted", // only worker can perform this action
      "enrouted", // only worker can perform this action
      "arrived", // only worker can peform this action
      "atLocation", // only client can perform this action
      "startjob", // only worker can perform this action
      'confirmjob',
      'schedule',
      "inprogress", // only worker can perfom this action
      "job end",
      "completed", //only client can perform this action
      "rejected", // only worker can perfom this action
      "cancel" // only client can perform this action
    ],
    default: "pending"
  },
  jobLocation: { type: locateSchema, required: true },
  jobAddress: { type: String},
  payment: {
        method: { type: String, default: 'cash' },
        material: { type: Number, default: 0 },
        labour: { type: Number, default: 0 },
        transportation: {type: Number, default: 500},
        others: { type: Number, default: 0 },
        currency: { type: String, enum: ["NGN", "USD", "EUR"]}
  },
  cancelReason: {
    canceller: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
    reason: { type: String },
    time: { type: Date }
  },
  conversation: [
    {
      senderId: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
      receiverId: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
      message: { type: String },
      attachment: [
        {
          document: { type: String}
        }
      ],
      isRead: { type: Boolean, default: false },
      isEdited: { type: Boolean, default: false },
      createdAt: { type: Date, default: Date.now() },
      updatedAt: { type: Date, default: Date.now() }
    }
  ],
  isRequested: {type: Boolean, default: true},
  isAvailable: { type: Boolean, default: true },
  created: { type: Date, default: Date.now() },
  updated: { type: Date, default: Date.now() }
});

bookingSchema.plugin(mongoosePaginate);
export default mongoose.model('Booking', bookingSchema)