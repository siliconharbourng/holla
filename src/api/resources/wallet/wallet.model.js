 import mongoose from 'mongoose';

 const {Schema} = mongoose;

const walletSchema = new Schema({
    user: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
    balanace: { type: Number, default: 0 },
    currency: { type: String, enum: ['NGN', 'USD'], default: "NGN" },
    transaction: [
        {
            amount: { type: Number},
            title: { type: String },
            status: { type: String, enum: ['pending', 'success', 'failed'], default: 'pending' },
            comment: { type: String },
            from: { type: String },
            to: { type: String },
            date: {type: Date, default: Date.now()}
        }
    ]
})
 
export default mongoose.model('Wallet', walletSchema);