import User from '../users/users.model'
import Wallet from './wallet.model';
import authService from '../auth/auth.service';
import { date } from 'joi';


export default {
    async allTransactions(req, res) {
        let wallet;
        try {
            const { id } = req.params
            const user = await User.findOne({ _id: id })
            if (user) {
                wallet = await Wallet.findOne({ user: id })
                if (wallet === null) {
                    wallet = await Wallet.create({
                        user: id
                    })
                    res.status(200).json(wallet);
                }
                return res.status(200).json(wallet);
            } else {
                return res.status(404).json({message: "No User found"});
            }
        } catch (error) {
            console.log(error)
        }
    },

    // async TopUp(req, res){
    //     try {
            
    //     } catch (error) {
    //         console.log(error)
    //     }
    // } 

    async Transfer(req, res) {
        try {
            const { id } = req.params
            const { value, error } = authService.validateTransfer(req.body); 
           if (error) {
               res.status(400).json(error)
           }  
            const body = {
                amount: value.amount,
                comment: value.comment,
                to:value.to,
            }
            const sender = await Wallet.findOne({ user: id})
            const receiver = await Wallet.findOne({ user: body.to })
            if (sender) {
                if (sender.balanace > body.amount && receiver) {
                    // update sender's balance 
                    const senderBalance = sender.balanace - body.amount
                    // update receiver's balance
                    const receiverBalance = receiver.balanace + body.amount
                    await Wallet.updateOne({ user: id }, {
                        $set: {
                            balanace: senderBalance
                        },
                        $push: {
                            transaction: {
                                $each: [
                                   {
                                    amount: value.amount,
                                    title: 'Fund Transfer',
                                    status: "success",
                                    comment: value.comment,
                                    from: id,
                                    to: value.to, 
                                    date: Date.now()
                                    }
                                ],
                                $position: 0
                            }
                        }
                    })
                    await Wallet.updateOne({ user: body.to }, {
                        $set: {
                            balanace: receiverBalance
                        },
                        $push: {
                            transaction: {
                                $each: [
                                   {
                                    amount: value.amount,
                                    title: 'Fund Received',
                                    status: "success",
                                    comment: value.comment,
                                    from: id,
                                    to: value.to, 
                                    date: Date.now()
                                    }
                                ],
                                $position: 0
                            }
                        }
                    })
                    res.status(200).json({message: "Transaction Successful"})
                } else {
                    await Wallet.updateOne({
                        user: id
                    }, {
                            $push: {
                            transaction: {
                                    $each: [
                                    {
                                            amount: value.amount,
                                            title: 'Fund Transfer',
                                            status: "failed",
                                            comment: value.comment,
                                            from: id,
                                            to: value.to, 
                                            date: Date.now()
                                    }
                                ],
                                $position: 0
                            }
                        }
                    })
                    res.status(404).json({message: sender.balanace < body.amount ? "INSUFFICEINT FUND" : "TRANSACTION ERROR" })
                }
                // const wallet = await Wallet.findOneAndUpdate({ user: id }, body, {new: true})
                
            } else {
                return res.status(404).json({message: "No Account found"});
            }
        } catch (error) {
            console.log(error)
        }
    }


}

