import express from 'express';
import walletController from './wallet.controller';

export const walletRouter = express.Router()

walletRouter.get('/:id', walletController.allTransactions);
walletRouter.post('/:id', walletController.Transfer);