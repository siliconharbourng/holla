import User from '../users/users.model';

export default {
    async POST_rating(req, res) {
        try {
            const { id } = req.params;
            await User.updateOne(
              {
                _id: id
              },
              {
                $push: {
                  reviews: {
                      $each: [
                          {
                              user: req.body.user,
                              rating: req.body.rating,
                              comment: req.body.comment,
                              date: Date.now()
                          }
                      ],
                      $position: 0
                  }
                }
              }
            ).then(() => { return res.status(200).json({ message: "User reviewed successfully" }) })
            .catch(err => { return res.status(500).json(err) });
        } catch (error) {
            console.error(error);
            return res.status(500).json(error)
        }
    },

    async GET_rating(req, res) {
        try {
            const { id } = req.params;
            const reviews = await User.findOne({ _id: id}).select('reviews');
            return res.status(200).json(reviews);
        } catch (error) {
            console.error(error);
            return res.status(500).json(error)
        }
    }
}