import express from 'express';
import ratingController from './rating.controller';

export const ratingRouter = express.Router();

ratingRouter.post('/:id', ratingController.POST_rating);
ratingRouter.get('/:id', ratingController.GET_rating);