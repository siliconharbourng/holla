import User from '../users/users.model';

export default {
	async ussdRequest(req, res) {
		try {
            const { sessionId, serviceCode, phoneNumber, text } = req.body;
			let phonenumber = '0' + phoneNumber.split('+234')[1];
			await User.findOne({ phone: phonenumber })
				.then((user) => {
					if (text === '') {
						let response = `CON Hello from hollaJob 
                                        Get workers near you with no stress
                                        Select:

                                        1. Request Worker
                                        2. info
                                        3. help`;
						res.status(200).send(response);
                    } else if (text === '1') {
                        let response = `CON 
                                        1. Request Worker
                                        2. Job Completion
                                        3. Rewards
                                        4. Terms and conditions
                                        5. Refer Friends
                                        0. Main menu
                                        #. back`;
						res.status(200).send(response);
                    } else if (text === '1*1') {
                        let response = `CON Select Type of worker you require

                                        1. Plumber
                                        2. Bricklayer
                                        3. Electrician Indoor
                                        4. Electrician Outdoor
                                        5. Electrician Building Projects
                                        6. Graphic Design
                                        7. Printing
                                        8. Tailor
                                        9. more
                                        #. back`;
						res.status(200).send(response);
                    } else {
                        let response = 'END UNKNOWN APPLICATION'
                        res.status(400).send(response);
                    }
				})
				.catch((err) =>
					res.status(400).send('END You are not yet a user of hollajobs', err),
				);
		} catch (error) {
			console.log(error);
		}
	},
};
