import express from 'express';
import ussdController from './ussd.controller';

const ussdRouter = express.Router();

ussdRouter.post('*', ussdController.ussdRequest)

export default ussdRouter;