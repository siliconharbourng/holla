import express from 'express';
import bookingChatController from './bookingChat.controller';

export const bookChatRouter = express.Router();

bookChatRouter.post('/:id/:senderId/:receiverId', bookingChatController.sendMessage);
bookChatRouter.get('/:id/:senderId/:receiverId', bookingChatController.AllMessage);
bookChatRouter.patch('/', bookingChatController.editMessage);
bookChatRouter.delete('/', bookingChatController.deleteMessage);

