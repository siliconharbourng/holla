import Booking from '../booking/booking.model';

export default {
  async sendMessage(req, res) {
    try {
      const { id, senderId, receiverId } = req.params
      const job = await Booking.find({
        $or: [
          { isAvailable: true, _id: id, seekersId: senderId, workersId: receiverId },
          { isAvailable: true, _id: id, seekersId: receiverId, workersId: senderId }
        ]
      });
      await Booking.updateOne(
        {
          _id: job[0]._id
        },
        {
          $push: {
            conversation: {
              senderId: senderId,
              receiverId: receiverId,
              message: req.body.message
            }
          }
        }
      )
        .then(()=> {
          return res.status(200).json({ message: "message sent"});
        })
        .catch(error => {
          return res.status(404).json(error);
        });
      
    } catch (error) {
      console.error(error);
      res.status(500).json(error);
    }
  },

  async editMessage(req, res) {
    try {
      return res.status(200).json({ message: "message edited" });
    } catch (error) {
      console.error(error);
      res.status(500).json(error);
    }
  },

  async deleteMessage(req, res) {
    try {
      return res.status(200).json({ message: "message deleted" });
    } catch (error) {
      console.error(error);
      res.status(500).json(error);
    }
  },

  async AllMessage(req, res) {
    try {
      const { id, senderId, receiverId } = req.params
      const job = await Booking.find({
        $or: [
          { isAvailable: true, _id: id, seekersId: senderId, workersId: receiverId },
          { isAvailable: true, _id: id, seekersId: receiverId, workersId: senderId }
        ]
      }).select("conversation");
      return res.status(200).json(job)
    } catch (error) {
      console.error(error);
      res.status(500).json(error);
    }
  }
};
