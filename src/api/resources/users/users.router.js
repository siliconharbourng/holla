import express from 'express';
import usersController from './users.controller';
import passport from 'passport';
import multer from 'multer';

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/');
    },
    filename: function (req, file, cb) {
        const name = file.originalname + new Date().toString() + '.jpg';
        const fileName = name.split(' ').join('')
        cb(null, fileName);
    }
});

const upload = multer({ storage: storage});

export const usersRouter = express.Router();

usersRouter.get('/', passport.authenticate('jwt', {session:false}), usersController.me);
usersRouter.get('/users', usersController.users);
usersRouter.get('/all-users/:id', usersController.one_user);
usersRouter.put('/', passport.authenticate('jwt', {session:false}), usersController.updateProfile);
usersRouter.patch('/:id', upload.single('avatar'), usersController.completeRegs)
usersRouter.post('/upload', upload.single('avatar'), passport.authenticate('jwt', {session:false}), usersController.upload_profile_pics)
usersRouter.post('/:id/skill', usersController.skill);
usersRouter.get('/unverified-user', usersController.unverified_kyc);
usersRouter.post('/verify_skill/:id/:skill', usersController.verify_user_skill);
