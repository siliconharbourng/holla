import mongoose, { Mongoose } from 'mongoose';
// import slug from 'mongoose-slug-generator';

const options = {
	separator: '-',
	lang: 'en',
	truncate: 50,
};

// mongoose.plugin(slug, options)

const { Schema } = mongoose;

const locateSchema = new mongoose.Schema({
	type: { type: String, enum: ['Point'], required: true, default: 'Point' },
	coordinates: {
		type: [Number],
		index: { type: '2dsphere' },
		default: [0.0, 0.0],
	},
});

const userSchema = new Schema({
	avatar: { type: String, default: '' },
	firstName: { type: String, default: '' },
	lastName: { type: String, default: '' },
	password: { type: String },
	my_location: {
		type: locateSchema,
		default: {
			coordinates: [0.0, 0.0],
			type: 'Point',
		},
	},
	accept_discliamer: { type: Boolean },
	slug: { type: String, slug: ['firstName', 'lastName'] },
	sex: { type: String, enum: ['male', 'female', 'none'], default: 'none' },
	email: { type: String },
	phone: { type: String },
	country_code: { type: String, default: '+234' },
	isConfirmed: { type: Boolean, default: false },
	address: { type: String, default: '' },
	refcode: { type: String },
	isSkilled_verified: { type: Boolean, default: false },
	isKYC_verified: { type: Boolean, default: false },
	referedby: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
	refered: [
		{
			refered: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
		},
	],
	reviews: [
		{
			user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
			rating: { type: Number, min: 1, max: 5, default: 1 },
			comment: { type: String, default: '' },
			date: { type: Date, default: Date.now() },
		},
	],
	workersRequested: [
		{ requestId: { type: mongoose.Schema.Types.ObjectId, ref: 'Booking' } },
	],
	jobOffer: [
		{ jobId: { type: mongoose.Schema.Types.ObjectId, ref: 'Booking' } },
	],
	rights: {
		type: String,
		enum: ['admin', 'super-admin', 'worker', 'client', 'sponsor'],
	},
	skills: [
		{
			skills: { type: String },
			category: { type: String },
			verified: {
				type: String,
				enm: ['pending', 'approved', 'rejected', 'suspended'],
				default: 'pending',
			},
		},
  ],
  verified_skill: [
    {
      skill: { type: String },
      category: { type: String },
    }
  ],
	notifications: [
		{
			senderId: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
			message: { type: String, default: '' },
			created: { type: Date, default: Date.now() },
			isread: { type: Boolean, default: false },
			updated: { type: Date },
		},
	],
	created_date: { type: Date, default: Date.now() },
	updated_date: { type: Date, default: Date.now() },
});

userSchema.index({ my_location: '2dsphere' });

export default mongoose.model('User', userSchema);
