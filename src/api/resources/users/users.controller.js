import User from './users.model';
import fs from 'fs';
import jwtDecode from 'jwt-decode';
import authService from '../auth/auth.service';

export default {
	async me(req, res) {
		try {
			const token = req.headers.authorization.split('bearer ')[1];
			const decoded = jwtDecode(token);
			const user = await User.findOne({ _id: decoded.id })
				.populate(
					'workersRequested.requestId workersRequested.requestId.workersId',
				)
				.populate('reviews.user', 'avatar firstName lastName')
				.populate('jobOffer.jobId');
			if (!user) {
				return res
					.status(400)
					.json({ message: 'User not found' || "User doesn't Exist" });
			} else {
				return res.status(200).json(user);
			}
		} catch (error) {
			console.error(error);
			return res.status(500).json({ message: 'Server Error' });
		}
	},

	async one_user(req, res) {
		try {
			const { id } = req.params;
			const user = await User.findOne({ _id: id })
				.populate(
					'workersRequested.requestId workersRequested.requestId.workersId',
				)
				.populate('reviews.user', 'avatar firstName lastName')
				.populate('jobOffer.jobId');
			if (!user) {
				return res
					.status(400)
					.json({ message: 'User not found' || "User doesn't Exist" });
			} else {
				return res.status(200).json(user);
			}
		} catch (error) {
			console.error(error);
			return res.status(500).json({ message: 'Server Error' });
		}
	},

	async skill(req, res) {
		try {
			const { id } = req.params;
			const skill = await User.updateOne(
				{
					_id: id,
				},
				{
					$push: {
						skills: {
							$each: [
								{
									skill: req.body.skill,
								},
							],
							$position: 0,
						},
					},
				},
			);
			return res
				.status(200)
				.json({ message: 'Skill has been added successfully', skill });
		} catch (error) {
			console.error(error);
		}
	},

	async completeRegs(req, res) {
		try {
			const { id } = req.params;
			await User.updateOne(
				{
					_id: id,
				},
				{
					$set: {
						avatar: req.file.path,
						firstName: req.body.firstName,
						lastName: req.body.lastName,
					},
				},
			);
			await User.updateOne(
				{
					refcode: req.body.refcode,
				},
				{
					$push: {
						refered: {
							$each: [
								{
									refered: id,
								},
							],
							$position: 0,
						},
					},
				},
			);
			res.status(200).json({ message: 'Registeration complete' });
			// console.log(req.body, req.file);
		} catch (error) {
			console.error(error);
			return res.status(500).json({ message: 'Server Error' });
		}
	},

	async updateProfile(req, res) {
		try {
			const token = req.headers.authorization.split('bearer ')[1];
			const decoded = jwtDecode(token);
			const { value, error } = authService.validate_profile_update(req.body);
			if (error) {
				res.status(400).json(error);
			}
			const body = {
				firstName: value.first_name,
				lastName: value.last_name,
				sex: value.sex,
				email: value.email,
				phone: value.phone,
			};
			const user = await User.findOneAndUpdate({ _id: decoded.id }, body, {
				new: true,
			});
			if (!user) {
				return res.status(400).json({ message: 'No user found' });
			} else {
				return res
					.status(200)
					.json({ success: 'Profile Updated successfully', user });
			}
		} catch (error) {
			console.log(error);
		}
	},

	async upload_profile_pics(req, res) {
		try {
			const token = req.headers.authorization.split('bearer ')[1];
			const decoded = jwtDecode(token);
			const photo = await User.findOne({ _id: decoded.id });
			const pathUrl = photo.avatar.split('/')[0];
			if (pathUrl === 'uploads') {
				const path = `./${photo.avatar}`;
				fs.unlinkSync(path);
			}
			const body = {
				avatar: req.file.path,
			};
			const user = await User.findOneAndUpdate({ _id: decoded.id }, body, {
				new: true,
			});
			return res
				.status(200)
				.json({ success: 'Profile Updated successfully', user });
		} catch (error) {
			console.log(error);
			res.status(500).json({ message: 'Server Error' });
		}
	},

	async users(req, res) {
		try {
			const users = await User.find().populate('verified_skills.skills');
			return res.status(200).json(users);
		} catch (error) {
			console.error(error);
			res.status(500).json(error);
		}
	},

	async unverified_kyc(req, res) {
		try {
			const users = await User.find();
			return res.status(200).json(users);
		} catch (error) {
			res.status(500).json({ message: 'Internal Server Error' });
		}
	},

	async verify_user_skill(req, res) {
		try {
			const { value, error } = authService.validate_skill(req.body);
			if (error) {
				res.status(400).json(error);
			}
			const { id, skill } = req.params;
			const user = await User.findOne({ _id: id })
			let verify_skill = {}
			for (let i = 0; i < user.skills.length; i++) {
				if (user.skills[i]._id == skill) {
					if (user.verified_skill.length > 0) {
						const comp = user.verified_skill.filter(data => data.skill === user.skills[i].skills)
						if (comp.length === 0) {
							verify_skill = user.skills[i];
							await User.updateOne(
								{
									_id: id,
									'skills._id': skill,
								},
								{
									$set: { 'skills.$.verified': value.status },
									$push: {
										verified_skill: {
											$each: [
												{
													skill: verify_skill.skills,
													category: verify_skill.category
												}
											]
										}
									}
								},
							)
								.then(async () => {
									res.status(200).json({ message: 'Skilled Verified' });
								})
								.catch((err) => res.status(400).json(err));
						}
						res.status(400).json({ message: "Skill has been approved" });
					} else {
						verify_skill = user.skills[i];
						await User.updateOne(
							{
								_id: id,
								'skills._id': skill,
							},
							{
								$set: { 'skills.$.verified': value.status },
								$push: {
									verified_skill: {
										$each: [
											{
												skill: verify_skill.skills,
												category: verify_skill.category
											}
										]
									}
								}
							},
						)
							.then(async () => {
								res.status(200).json({ message: 'Skilled Verified' });
							})
							.catch((err) => res.status(400).json(err));
					}
				}
			}
		} catch (error) {
			res.status(500).json({ message: 'Internal Server Error' });
		}
	},
};
