import Joi from 'joi';
import bcrypt from 'bcryptjs';

export default {
    ecryptpassword(plaintext) {
        const salt = bcrypt.genSaltSync(10);
        return bcrypt.hashSync(plaintext, salt)
    },

    comparePassword(plaintext, ecryptpassword) {
        return bcrypt.compareSync(plaintext, ecryptpassword)
    },

    validateWorkerRegister(body) {      
        const schema = Joi.object().keys({
            firstName: Joi.string().required(),
            lastName: Joi.string().required(),
            phone: Joi.string().required(),
            email: Joi.string().email().required(),
            sex: Joi.string().valid('male', 'female').required(),
            password: Joi.string().required(),
            accept_discliamer: Joi.boolean().required(),
            address: Joi.string().optional()
        });
        const { value, error } = Joi.validate( body, schema);  
        if (error && error.details) {
            return { error }
        }
            return { value }
    },

    validateTransfer(body) {
        const schema = Joi.object().keys({
            amount: Joi.number().integer().required(),
            to: Joi.string().required(),
            comment: Joi.string().optional()
        });
        const { value, error } = Joi.validate( body, schema);  
        if (error && error.details) {
            return { error }
        }
            return { value }
    },

    validateClientRegister(body) {      
        const schema = Joi.object().keys({
            firstName: Joi.string().required(),
            lastName: Joi.string().required(),
            phone: Joi.string().required(),
            email: Joi.string().email().required(),
            password: Joi.string().required(),
            address: Joi.string().optional(),
            accept_discliamer: Joi.boolean().required()
        });
        const { value, error } = Joi.validate( body, schema);  
        if (error && error.details) {
            return { error }
        }
            return { value }
    },

    validatelogin(body) {      
        const schema = Joi.object().keys({
            user: Joi.string().required(),
            password: Joi.string().required(),
            rights: Joi.string().valid('admin', 'super-admin', 'worker', 'client', 'sponsor').required(),
        });
        const { value, error } = Joi.validate( body, schema);  
        if (error && error.details) {
            return { error }
        }
            return { value }
    },
    
    validateRegisterOtp(body) {      
        const schema = Joi.object().keys({
            phone_no: Joi.string().required(),
            email: Joi.string().email().required(),
            country_code: Joi.string().required(),
            rights: Joi.string().valid('admin', 'super-admin', 'worker', 'client', 'sponsor').required(),
        });
        const { value, error } = Joi.validate( body, schema);  
        if (error && error.details) {
            return { error }
        }
            return { value }
    },

    validateloginOtp(body) {      
        const schema = Joi.object().keys({
            phone_no: Joi.string().required(),
            country_code: Joi.string().required(),
            rights: Joi.string().valid('admin', 'super-admin', 'worker', 'client', 'sponsor').required(),
        });
        const { value, error } = Joi.validate( body, schema);  
        if (error && error.details) {
            return { error }
        }
            return { value }
    },

    validateOtp(body) {      
        const schema = Joi.object().keys({
            otp: Joi.string().required(),
        });
        const { value, error } = Joi.validate( body, schema);  
        if (error && error.details) {
            return { error }
        }
            return { value }
    },

    validatePreReg(body) {
        const schema = Joi.object().keys({
            firstName: Joi.string().required(),
            lastName: Joi.string().required(),
            email: Joi.string().email().required(),
            phone: Joi.string().required()
        });
        const { value, error } = Joi.validate( body, schema);  
        if (error && error.details) {
            return { error }
        }
            return { value }
    },

    validateBooking(body) {
        const schema = Joi.object().keys({
            seekersId: Joi.string().required(),
            sender: Joi.string(),
            skill: Joi.string(),
            reqesting_for: Joi.object().keys({
                name: Joi.string().allow(null, '').optional(),
                sex: Joi.string().allow(null, '').optional(),
                phone: Joi.string().allow(null, '').optional(),
            }),
            jobLocation: Joi.object().keys({
                coordinates: Joi.array().required()
            }),
            jobAddress: Joi.string().optional(),
        });
        const { value, error } = Joi.validate(body, schema);
        if (error && error.details) {
            return { error }
        }
        return { value }
    },
    

    validateStatus(body) {
        const schema = Joi.object().keys({
            status: Joi.string().valid('pending', 'accepted', 'arrived',"enrouted","atLocation", 'startjob', 'confirmjob', 'inprogress','job end', 'completed', 'rejected', 'cancel'),
            cancelReason: Joi.object().keys({
                canceller: Joi.string().optional(),
                reason: Joi.string().optional(),
                time: Joi.date().optional()
            }),
            payment: Joi.object().keys({
                method: Joi.string().optional(),
                material: Joi.number().integer().optional(),
                labour: Joi.number().integer().optional(),
                others: Joi.number().integer().optional(),
                currency: Joi.string().valid("NGN", "USD", "EUR")
            }),
            isAvailable: Joi.boolean().optional(),
            updated: Joi.date().optional()
        });
        const { value, error } = Joi.validate(body, schema);
        if (error && error.details) {
            return { error }
        }
        return { value }
    },

    validate_registeration(body) {
        const schema = Joi.object().keys({
            first_name: Joi.string().required(),
            last_name: Joi.string().required(),
            email: Joi.string().required(),
            phone: Joi.string().required(),
            skill: Joi.array().optional(),
            terms: Joi.boolean().valid(true).required(),
            params: Joi.string().valid('client', 'worker').required()
        });
        const { value, error } = Joi.validate(body, schema);
        if (error && error.details) {
            return {error}
        }
        return {value}
    },

    validate_login(body) {      
        const schema = Joi.object().keys({
            phone: Joi.string().required(),
            params: Joi.string().valid('worker', 'client').required(),
        });
        const { value, error } = Joi.validate( body, schema);  
        if (error && error.details) {
            return { error }
        }
            return { value }
    },

    validate_skill(body) {      
        const schema = Joi.object().keys({
            status: Joi.string().valid('approved').required(),
        });
        const { value, error } = Joi.validate( body, schema);  
        if (error && error.details) {
            return { error }
        }
            return { value }
    },

    validate_social(body) {
        const schema = Joi.object().keys({
            first_name: Joi.string().required(),
            last_name: Joi.string().required(),
            email: Joi.string().required(),
            avatar: Joi.string().required(),
            params: Joi.string().valid('worker', 'client').required(),
        });
        const { value, error } = Joi.validate(body, schema);
        if (error && error.details) {
            return {error}
        }
        return {value}
    },

    validate_profile_update(body) {
        const schema = Joi.object().keys({
            first_name: Joi.string().required(),
            last_name: Joi.string().required(),
            email: Joi.string().required(),
            sex: Joi.string().valid('male', 'female', 'none').required(),
            phone: Joi.string().required(),
        });
        const { value, error } = Joi.validate(body, schema);
        if (error && error.details) {
            return {error}
        }
        return {value}
    }
}