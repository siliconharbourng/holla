import mongoose from 'mongoose';
const { Schema } = mongoose;

const otpSchema = new Schema({
    code: { type: Number },
    phone: {type: Number},
    country_code: { type: String },
    created: {type: Number}
});


export default mongoose.model('Otp', otpSchema);