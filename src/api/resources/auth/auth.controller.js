import authService from './auth.service';
import User from '../users/users.model';
import Otp from './otp';
import createHash from 'hash-generator';
import jwt from '../../helpers/jwt';
import otpGenerator from 'otp-generator';
import nodemailer from 'nodemailer';

var hashLength = 8;
var hash = createHash(hashLength);

var transport = nodemailer.createTransport({
	host: 'frc.frcpay.com',
	port: 587,
	secure: false,
	// logger: true,
	// debug: true,
	// ignoreTLS: true,
	auth: {
		user: 'no-reply@hollajobs.com',
		pass: '3bF5BDadAzxcQSq',
	},
});

// var transport = nodemailer.createTransport({
//     host: 'smtp.gmail.com',
//     secure: true,
//     logger: true,
//     debug: true,
//     ignoreTLS: true,
//     auth: {
//         user: "olagunjuadewale5@gmail.com",
//         pass: "olagunju005"
//     }
// });

export default {
	async workersReg(req, res) {
		try {
			const { value, error } = authService.validateWorkerRegister(req.body);
			if (error) {
				res.status(400).json(error);
			}
			const codegen =
				value.firstName.substring(0, 5).toUpperCase() + hash.toUpperCase();
			const code = codegen.substring(0, 8);

			const encyptpass = authService.ecryptpassword(value.password);
			const phone = await User.findOne({ phone: value.phone });
			const email = await User.findOne({ email: value.email });
			if (phone) {
				return res
					.status(400)
					.json({
						message: 'Someone has already registered with this Phone Number',
					});
			} else if (email) {
				return res
					.status(400)
					.json({ message: 'Someone has registered with this email address' });
			} else if (value.accept_discliamer == false) {
				return res
					.status(400)
					.json({ message: 'Make Sure you accept the terms  of use' });
			} else {
				const user = await User.create({
					firstName: value.firstName,
					lastName: value.lastName,
					phone: value.phone,
					email: value.email,
					sex: value.sex,
					password: encyptpass,
					refcode: code,
					rights: 'worker',
					accept_discliamer: value.accept_discliamer,
					address: value.address,
				});
				const token = jwt.issue({ id: user._id }, '365d');
				return res.json({ success: true, user: user._id, token });
			}
		} catch (error) {
			console.error(error);
			return res.status(500).send(error);
		}
	},

	async ClientReg(req, res) {
		try {
			const { value, error } = authService.validateClientRegister(req.body);
			if (error) {
				res.status(400).json(error);
			}

			const codegen = hash.toUpperCase();
			const code = codegen.substring(0, 4);
			const encyptpass = authService.ecryptpassword(value.password);
			const phone = await User.findOne({ phone: value.phone });
			const email = await User.findOne({ email: value.email });
			if (phone) {
				return res
					.status(400)
					.json({
						message: 'Someone has already registered with this Phone Number',
					});
			} else if (email) {
				return res
					.status(400)
					.json({ message: 'Someone has registered with this email address' });
			} else if (value.accept_discliamer == false) {
				return res
					.status(400)
					.json({ message: 'Make Sure you accept the terms  of use' });
			} else {
				const user = await User.create({
					firstName: value.firstName,
					lastName: value.lastName,
					phone: value.phone,
					email: value.email,
					password: encyptpass,
					address: value.address,
					refcode: code,
					rights: 'client',
					accept_discliamer: value.accept_discliamer,
				});
				const token = jwt.issue({ id: user._id }, '365d');
				return res.json({ success: true, user: user._id, token });
			}
		} catch (error) {
			console.error(error);
			return res.status(500).send(error);
		}
	},

	async signin(req, res) {
		try {
			const { value, error } = authService.validatelogin(req.body);
			if (error) {
				res.status(400).json(error);
			}

			const user =
				(await User.findOne({ phone: value.user, rights: value.rights })) ||
				(await User.findOne({ email: value.user, rights: value.rights }));
			if (!user) {
				return res
					.status(401)
					.json({ message: 'Invalid Phone Number or Email address' });
			}

			const password = authService.comparePassword(
				value.password,
				user.password,
			);
			if (!password) {
				return res.status(401).json({ message: 'Your password is incorrect' });
			}
			const token = jwt.issue({ id: user._id }, '365d');
			return res.json({ user: user._id, token });
		} catch (error) {
			console.error(error);
			return res.status(500).send(error);
		}
	},

	authenticate(req, res) {
		return res.json(req.user);
	},

	async RegisterWithOtp(req, res) {
		try {
			const { value, error } = authService.validateRegisterOtp(req.body);
			if (error) {
				res.status(400).json(error);
			}
			var code = otpGenerator.generate(4, {
				upperCase: false,
				alphabets: false,
				digits: true,
				specialChars: false,
			});
			const otpuser = {
				code: code,
				phone: value.phone_no,
				country_code: value.country_code,
				created: Date.now(),
			};
			const codegen = hash.toUpperCase();
			const referal = codegen.substring(0, 6);
			const phone = await User.findOne({ phone: value.phone_no });
			const email = await User.findOne({ email: value.email });
			if (phone) {
				return res
					.status(400)
					.json({
						message: 'Someone has already registered with this Phone Number',
					});
			} else if (email) {
				return res
					.status(400)
					.json({ message: 'Someone has registered with this email address' });
			} else {
				Otp.create(otpuser).then(async (resp) => {
					const user = await User.create({
						phone: value.phone_no,
						email: value.email,
						country_code: value.country_code,
						refcode: referal,
						rights: value.rights,
					});
					// return res.status(200).json({message: "Check you email for your OTP", app: user.rights});
					const message = {
						from: 'no-reply@hollajobs.com',
						to: value.email,
						subject: 'Hollajobs OTP',
						text: `Please use the OTP code: ${code} to complete your registration. It would expire within 3 minutes`,
					};
					transport.sendMail(message, async function (err, info) {
						if (err) {
							console.log(err);
						}
						return res
							.status(200)
							.json({
								message: 'Check you email for your OTP',
								app: user.rights,
							});
					});
				});
			}
		} catch (error) {
			console.error(error);
			return res.status(500).send(error);
		}
	},

	async signinWithOtp(req, res) {
		try {
			const { value, error } = authService.validateloginOtp(req.body);
			if (error) {
				res.status(400).json(error);
			}
			var code = otpGenerator.generate(4, {
				upperCase: false,
				alphabets: false,
				digits: true,
				specialChars: false,
			});
			const otpuser = {
				code: code,
				phone: value.phone_no,
				country_code: value.country_code,
				created: Date.now(),
			};
			const user = await User.findOne({ phone: value.phone_no });
			if (!user.rights) {
				await User.updateOne(
					{ _id: user._id },
					{
						$set: {
							rights: value.rights,
						},
					},
				);
			}
			const phone = await User.findOne({
				phone: value.phone_no,
				rights: value.rights,
			});
			if (!phone) {
				return res
					.status(400)
					.json({
						message:
							'Phone Number does not exist can you please try and create a new account?',
					});
			} else {
				Otp.create(otpuser).then(async (resp) => {
					const member = await User.findOne({ phone: '0' + resp.phone });
					// return res.json({message: "Check you email for your OTP", app: value.rights});
					const message = {
						from: 'no-reply@hollajobs.com',
						to: member.email,
						subject: 'Hollajobs OTP',
						text: `Please use the OTP code: ${code} to login. It would expire within 3 minutes`,
					};
					transport.sendMail(message, function (err, info) {
						if (err) {
							console.log(err);
						} else {
							console.log(info);
							return res.json({
								message: 'Check you email for your OTP',
								app: value.rights,
							});
						}
					});
				});
			}
		} catch (error) {
			// console.error(error)
			return res.status(500).send(error);
		}
	},

	async verifyOtp(req, res) {
		try {
			const { value, error } = authService.validateOtp(req.body);
			if (error) {
				res.status(400).json(error);
			}
			const user = await Otp.findOne({ code: value.otp });
			if (user) {
				if (Date.now() - user.created > 300000) {
					return res.status(400).json({ message: 'Expired OTP' });
				} else {
					const member = await User.findOne({ phone: '0' + user.phone });
					member.isConfirmed
						? null
						: await User.findOneAndUpdate(
								{ _id: member._id },
								{ isConfirmed: true },
								{ new: true },
						  );
					const token = jwt.issue({ id: member._id }, '365d');
					return res.json({ user: member._id, token });
				}
			}
			return res.status(400).json({ message: 'Invalid OTP' });
		} catch (error) {
			console.error(error);
			return res.status(500).json(error);
		}
	},

	async preRegister(req, res) {
		try {
			const { value, error } = authService.validatePreReg(req.body);
			if (error) {
				res.status(400).json(error);
			}
			const phone = await User.findOne({ phone: value.phone });
			const email = await User.findOne({ email: value.email });
			if (phone) {
				return res
					.status(200)
					.json({
						error: true,
						message: 'Someone has already registered with this Phone Number',
					});
			} else if (email) {
				return res
					.status(200)
					.json({
						error: true,
						message: 'Someone has registered with this email address',
					});
			} else {
				User.create({
					firstName: value.firstName,
					lastName: value.lastName,
					email: value.email,
					phone: value.phone,
				});
				return res
					.status(200)
					.json({ error: false, message: 'You have Successfully Registered' });
			}
		} catch (error) {
			console.log(error);
			return res.status(500).json(error);
		}
	},

	async user_registeration(req, res) {
		try {
			const { value, error } = authService.validate_registeration(req.body);
			if (error) {
				res.status(400).json(error);
			}
			var code = otpGenerator.generate(4, {
				upperCase: false,
				alphabets: false,
				digits: true,
				specialChars: false,
			});
			const otpuser = {
				code: code,
				phone: value.phone,
				created: Date.now(),
			};
			const codegen = hash.toUpperCase();
			const referal = codegen.substring(0, 6);
			const phone = await User.findOne({ phone: value.phone });
			const email = await User.findOne({ email: value.email });
			if (phone) {
				return res
					.status(400)
					.json({
						message: 'Someone has already registered with this Phone Number',
					});
			} else if (email) {
				return res
					.status(400)
					.json({ message: 'Someone has registered with this email address' });
			} else {
				Otp.create(otpuser).then(async (resp) => {
					const user = await User.create({
						firstName: value.first_name,
						lastName: value.last_name,
						phone: value.phone,
						email: value.email,
						refcode: referal,
						accept_discliamer: value.terms,
						rights: value.params,
                    });
                    if (value.skill.length > 0) {
                        await User.updateOne(
                            {
                                _id: user._id,
                            },
                            {
                                $push: {
                                    skills: {
                                        $each: value.skill
                                    },
                                },
                            },
                        );
					}
					// return res.status(200).json({message: "Check you email for your OTP", app: user.rights});
					const message = {
						from: 'no-reply@hollajobs.com',
						to: value.email,
						subject: 'Hollajobs OTP',
						text: `Please use the OTP code: ${code} to complete your registration. It would expire within 3 minutes`,
					};
					transport.sendMail(message, async function (err, info) {
						if (err) {
							console.log(err);
						}
						return res
							.status(200)
							.json({
								message: 'Check you email for your OTP',
							});
					});
				});
			}
		} catch (error) {
			console.error(error);
			return res.status(500).send(error);
		}
	},

	async user_login(req, res) {
		try {
			const { value, error } = authService.validate_login(req.body);
			if (error) {
				res.status(400).json(error);
			}
			var code = otpGenerator.generate(4, {
				upperCase: false,
				alphabets: false,
				digits: true,
				specialChars: false,
			});
			const otpuser = {
				code: code,
				phone: value.phone,
				created: Date.now(),
			};
			const phone = await User.findOne({
				phone: value.phone,
				rights: value.params,
			});
			if (!phone) {
				return res
					.status(400)
					.json({
						message:
							'Phone Number does not exist can you please try and create a new account?',
					});
			} else {
				Otp.create(otpuser).then(async (resp) => {
					const member = await User.findOne({ phone: '0' + resp.phone });
					// return res.json({message: "Check you email for your OTP", app: value.rights});
					const message = {
						from: 'no-reply@hollajobs.com',
						to: member.email,
						subject: 'Hollajobs OTP',
						text: `Please use the OTP code: ${code} to login. It would expire within 3 minutes`,
					};
					transport.sendMail(message, function (err, info) {
						if (err) {
							console.log(err, info);
						} else {
							return res.json({
								message: 'Check you email for your OTP',
							});
						}
					});
				});
			}
		} catch (error) {
			console.error(error);
			return res.status(500).send(error);
		}
	},

	async social_login(req, res) {
		try {
			const { value, error } = authService.validate_social(req.body);
			if (error) {
				res.status(400).json(error);
			}
			const codegen = hash.toUpperCase();
			const referal = codegen.substring(0, 6);
			await User.findOne({ email: value.email }).then(async (resp) => {
				if (resp) {
					if (resp.rights !== value.params) {
						return res
							.status(400)
							.json({
								message: `This account does not exist as a ${value.params}`,
							});
					}
					const token = jwt.issue({ id: resp._id }, '365d');
					return res.json({ user: resp._id, token });
				} else {
					const user = await User.create({
						firstName: value.first_name,
						lastName: value.last_name,
						email: value.email,
						refcode: referal,
						avatar: value.avatar,
						rights: value.params,
					});
					const token = jwt.issue({ id: user._id }, '365d');
					return res.json({ user: user._id, token });
				}
			});
		} catch (error) {
			console.log(error);
			return res.status(500).json(error);
		}
	},
};
