import express from 'express'
import passport from 'passport';
import authController from './auth.controller';

export const authRouter = express.Router();

authRouter.post('/register/worker', authController.workersReg);
authRouter.post('/register/client', authController.ClientReg);
authRouter.post('/signin', authController.signin);
authRouter.post('/registration', authController.user_registeration)
authRouter.post('/login', authController.user_login)
authRouter.post('/social', authController.social_login)
authRouter.post('/signinotp', authController.signinWithOtp);
authRouter.post('/registerotp', authController.RegisterWithOtp);
authRouter.post('/preregistration', authController.preRegister);
authRouter.post('/verifyotp', authController.verifyOtp);
authRouter.get('/me', passport.authenticate('jwt', {session:false}), authController.authenticate);
