import express from 'express';
import logger from 'morgan';
import path from 'path';
import swaggerUi from 'swagger-ui-express';
import passport from 'passport';
import cookieParser from 'cookie-parser';
import { connect } from './config/db'; 
import { restApIRouter } from './api';
import { configJWTStrategy } from './api/middlewares/passport-jwt';
import swaggerDocument from '../src/config/swagger.json'
import  User  from './api/helpers/UserClass';
import _ from 'lodash';
import ussdRouter from './api/resources/ussd/ussd.router';

const app = express();
const PORT = process.env.PORT || 8000;
const HOSTNAME = '0.0.0.0';
const server = require('http').createServer(app);
const io = require('socket.io').listen(server);

// console.log(path.join(__dirname, '../uploads'))

connect();
app.use(cookieParser());
app.use('/uploads', express.static(path.join(__dirname, '../uploads')));
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({extended: true, limit: '50mb'}));

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  );
  if (req.method === 'OPTIONS') {
    console.log();
    
    res.header('Access-Control-Allow-Methods', 'PUT, PATCH, GET, POST, DELETE');
    return res.status(200).json({})
  }
  next();
})

app.use(logger('dev'));
app.use(passport.initialize())
configJWTStrategy()

// app.use('*', ussdRouter);
app.use('/api/v1', restApIRouter)
app.use('/doc',swaggerUi.serve, swaggerUi.setup(swaggerDocument, {
  explorer: true
}))

// app.get('/', (req, res) => res.json({ msg: 'Welcome to Holla Jobs and iwanwork Restful APIS' }));
app.use((req, res, next) => {
  const error = new Error('Not found');
  error.message = 'Invalid route';
  error.status = 404;
  next(error);
});
app.use((error, req, res, next) => {
  res.status(error.status || 500);
  return res.json({
    error: {
      message: error.message,
    },
  });
});


require('../src/api/helpers/stream')(io, User, _);
require('../src/api/helpers/private')(io);

server.listen(PORT, () => {
  console.log(`Server is running at PORT http://${HOSTNAME}:${PORT}`);
});
