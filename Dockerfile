FROM node:latest
LABEL key="Hollajob api version 1"

COPY . /src1

WORKDIR /src1

RUN npm install --production

EXPOSE 8000

CMD npm start
